function reply(t, m, p) {
    var r = JSON.stringify({t: t, m: m, p: p})
    console.log("WS out", r)
    websocket.send(r)
}

function handleRequest(f, m, p) {
    switch (m) {
        case "r":
            reply(f, m + "r", [myid, "b"])
            break

        case "rs":
            reply("hub", "l")
            break

        case "pr":
			var pio = p[0]
			var state = p[1]
		
			var c = (state == "1")
            if (pio == "24" || pio == "39") state = !state // buttons

			$(t("#pio_#{this.id}_#{this.pio}")({id: f, pio: pio})).checked = c
            break

        case "ser":
            var acc_d = Math.pow(2, 15) / 2
            var gyr_d = 245

            $("#results_" + f).innerHTML = t(
				"ACC: #{this.acc_x}, #{this.acc_y}, #{this.acc_z} [g] \
            	 GYR: #{this.gyr_x}, #{this.gyr_y}, #{this.gyr_z} [dps] \
				 TMP: #{this.temp} [\u00B0C]"
			)({
				acc_x: (p[0] / acc_d).toFixed(3), acc_y: (p[1] / acc_d).toFixed(3), acc_z: (p[2] / acc_d).toFixed(3),
				gyr_x: (p[3] / gyr_d).toFixed(3), gyr_y: (p[4] / gyr_d).toFixed(3), gyr_z: (p[5] / gyr_d).toFixed(3),
				temp: (p[6] / 16 + 25).toFixed(3)
			})
            break

        case "lr":
            setClients(p)
            break
    }
}

function appendLog(s) {
    console.log(s)
}

function setLed(c, pio, to) {
    reply(to, "p", ["" + pio, c.checked ? "1" : "0"])
}

function readSensor(to) {
    reply(to, "se")
}

function setClients(c) {
    clients = []
    types = []
    for (var i = 0; i < c.length; i++) {
        var s = c[i].split(' ');
        clients[i] = s[1];
        types[i] = s[0];
    }
	 
	var clientList = $("#clientList")
	clientList.innerHTML = ""
    for (var i = 0; i < clients.length; i++) {
        if (types[i] != 'm') continue;
		
		clientList.innerHTML += t(
		'<li>\
        <p>#{this.id}</p>\
        <label><input type="checkbox" id="pio_#{this.id}_40" onclick="setLed(this, 40, \'#{this.id}\')">LED1</label>\
        <label><input type="checkbox" id="pio_#{this.id}_41" onclick="setLed(this, 41, \'#{this.id}\')">LED2</label>\
        <label><input type="button" onclick="readSensor(\'#{this.id}\')" value="Read Sensor"></label>\
        <label><input disabled="disabled" type="checkbox" id="pio_#{this.id}_24">SW2</label>\
        <label><input disabled="disabled" type="checkbox" id="pio_#{this.id}_39">SW3</label>\
        <div id="results_#{this.id}"></div>\
        </li>'
		)({id: clients[i]})
    }
}

function init() {
    var id_length = 16
    myid = Math.round((Math.pow(36, id_length + 1) - Math.random() * Math.pow(36, id_length))).toString(36).slice(1)
	
	clients = []
	types = []
	
	// Example inputs!
    handleRequest("hub", "lr", [ "m example_client1", "m example_client2"])
	handleRequest("example_client1", "ser", ["100", "200", "1600", "4", "5", "6", "40"])
	handleRequest("example_client1", "pr", ["40", "1"])
	handleRequest("example_client2", "pr", ["24", "1"])
	
    websocket = new WebSocket("ws://localhost:8081/ws", "bccloud")

    websocket.onopen = function() {
        appendLog("CONNECTED")

        setInterval(function() {
            for (var i = 0; i < clients.length; i++) {
                // if (types[i] == 'm') reply(clients[i], "se")
            }
        }, 1000);
    };

    websocket.onmessage = function(evt) {
        var req = JSON.parse(evt.data)
        console.log("WS in:", evt.data)
        handleRequest(req.f, req.m, req.p)
    };

    websocket.onerror = function(evt) {
        appendLog("ERROR: " + evt.data)
    };

    websocket.onclose = function(evt) {
        appendLog("DISCONNECTED")
    };
}

window.addEventListener("load", init, false);