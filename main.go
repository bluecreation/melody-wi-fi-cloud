package main

import (
	"bluecreation.com/wificloud/hub"
	"flag"
	"fmt"
	"log"
	"net/http"
)

const (
	NAME    = "Melody Wi-Fi Cloud Demo"
	VERSION = NAME + " " + "v0.4"
)

var flags struct {
	Port    int
	Version bool
}

func parseFlags() {
	flag.IntVar(&flags.Port, "p", 8081, "the port to listen on")
	flag.BoolVar(&flags.Version, "v", false, "print the program version")
	flag.Parse()
}

func main() {
	parseFlags()

	if flags.Version {
		fmt.Println(VERSION)
	} else {
		var address = fmt.Sprintf(":%d", flags.Port)

		fmt.Printf("Listening on %v...\n", address)

		h := hub.NewCloudHub()
		http.Handle("/", h)
		log.Fatalf("Terminated with error: %v", http.ListenAndServe(address, nil))
	}
}
