package hub

import (
	"fmt"
	"github.com/gorilla/websocket"
	"log"
	"net/http"
	"time"
)

var upgrader = websocket.Upgrader{
	ReadBufferSize:  1024,
	WriteBufferSize: 1024,
	Subprotocols:    []string{"bccloud"},
	CheckOrigin:     func(r *http.Request) bool { return true },
}

// broadcastMessage sends the given message to all participants, which aren't the sending
// module, are not unregistered, and are not modules
func (h *CloudHub) broadcastMessage(from *CloudConnection, m *CloudMessage) {
	for _, c := range h.connections() {
		if c != from && c.ClientId != "" && c.ClientType != "m" {
			h.trySendMessage(c, m)
		}
	}
}

func (h *CloudHub) trySendMessage(to *CloudConnection, msg *CloudMessage) bool {
	if err := to.SendMessage(msg); err != nil {
		log.Println("Error sending message", msg, err)
		h.unregister(to)
		return false
	}

	return true
}

func (h *CloudHub) handleHubMessage(conn *CloudConnection, m *CloudMessage) {
	switch m.Message {
	case "rr":
		id := m.Parameters[0]
		clientType := m.Parameters[1]

		r := &CloudMessage{From: m.To, To: m.From, Message: "rs"}
		if h.register(conn, id, clientType) {
			r.Parameters = []string{"ok"}
		} else {
			r.Parameters = []string{"already registered"}
		}

		h.trySendMessage(conn, r)

	case "u":
		r := &CloudMessage{From: m.To, To: m.From, Message: "ur"}
		h.trySendMessage(conn, r)
		h.unregister(conn)

	case "l":
		r := h.prepareConnectedDevicesMessage()
		h.trySendMessage(conn, &r)
	}
}

func (h *CloudHub) prepareConnectedDevicesMessage() CloudMessage {
	r := CloudMessage{From: HUB_NAME, Message: "lr", Parameters: make([]string, len(h.connections()))}
	for i, c := range h.connections() {
		r.Parameters[i] = fmt.Sprintf("%v %v", c.ClientType, c.ClientId)
	}
	return r
}

func (h *CloudHub) announceConnStatus(conn *CloudConnection) {
	r := h.prepareConnectedDevicesMessage()
	h.broadcastMessage(conn, &r)
}

func (h *CloudHub) register(conn *CloudConnection, id string, clientType string) bool {
	if c, exists := h.id2conn[id]; exists {
		h.unregister(c)
	}

	log.Printf("Registering %q (%q) on %v", id, clientType, conn.RemoteAddr())

	conn.ClientId = id
	conn.ClientType = clientType

	h.id2conn[id] = conn
	h.conn2id[conn] = id

	h.announceConnStatus(conn)

	return true
}

func (h *CloudHub) unregister(conn *CloudConnection) {
	if id, ok := h.conn2id[conn]; ok {
		delete(h.id2conn, id)
		delete(h.conn2id, conn)

		h.announceConnStatus(conn)
	}

	conn.Close()
}

func (h *CloudHub) routeMessage(conn *CloudConnection, m *CloudMessage) {
	if m.To == HUB_NAME {
		h.handleHubMessage(conn, m)
	} else if m.To == "b" {
		h.broadcastMessage(conn, m)
	} else if conn, ok := h.id2conn[m.To]; ok {
		h.trySendMessage(conn, m)
	} else {
		log.Println("Couldn't route request:", m)
	}
}

func (h *CloudHub) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	log.Println("Got a new WS connection")

	c, err := upgrader.Upgrade(w, r, nil)
	if err != nil {
		log.Println(err)
		http.Error(w, http.ErrNotSupported.Error(), http.StatusNotAcceptable)
		return
	}

	go func() {
		conn := NewCloudConnection(c)
		if !h.trySendMessage(conn, &CloudMessage{From: "hub", Message: "r"}) {
			return
		}

		for {
			var m *CloudMessage
			if m, err = conn.ReceiveMessage(); err != nil {
				log.Printf("Connection receive error: %T, %s\n", err, err)
				h.unregister(conn)
				return
			}

			if id, ok := h.conn2id[conn]; ok {
				m.From = id
			}

			log.Printf("Got message: %+v\n", m)

			h.routeMessage(conn, m)
		}
	}()
}

func (c *CloudHub) connections() []*CloudConnection {
	result := make([]*CloudConnection, len(c.id2conn))
	i := 0
	for _, v := range c.id2conn {
		result[i] = v
		i++
	}
	return result
}

func (c *CloudHub) startPingTimer() {
	ch := time.Tick(5 * time.Second)
	for range ch {
		fmt.Println("tick")

		for _, cn := range c.connections() {
			if cn.PongPending {
				cn.Close()
			} else {
				cn.SendPing()
			}
		}
	}
}

// NewCloudHub creates and initializes a new cloud hub
func NewCloudHub() *CloudHub {
	h := &CloudHub{}
	h.conn2id = make(map[*CloudConnection]string)
	h.id2conn = make(map[string]*CloudConnection)

	go h.startPingTimer()

	return h
}
