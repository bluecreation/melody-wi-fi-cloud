package hub

import (
	"fmt"
	"github.com/gorilla/websocket"
	"log"
	"net"
	"time"
)

type CloudMessage struct {
	From       string   `json:"f,omitempty"`
	To         string   `json:"t,omitempty"`
	Message    string   `json:"m,omitempty"`
	Parameters []string `json:"p,omitempty"`
}

type CloudConnection struct {
	connection *websocket.Conn
	ClientId   string
	ClientType string
	PongPending bool
}

func NewCloudConnection(conn *websocket.Conn) (*CloudConnection) {
	c := &CloudConnection{connection: conn}
	c.connection.SetPongHandler(c.pongHandler)
	return c
}

func (c *CloudConnection) Close() {
	c.connection.Close()
}

func (c *CloudConnection) SendMessage(m *CloudMessage) error {
	log.Printf("WS Out: %+v", m)
	c.connection.SetWriteDeadline(time.Now().Add(1 * time.Second))
	return c.connection.WriteJSON(m)
}

func (c *CloudConnection) pongHandler(data string) error {
	fmt.Printf("Got Pong response from %v: %v\n", c.ClientId, data)
	c.PongPending = false
	return nil
}

func (c *CloudConnection) SendPing() error {
	log.Printf("WS Ping: %+v", c.ClientId)
	c.PongPending = true
	return c.connection.WriteControl(websocket.PingMessage, []byte("there?"), time.Now().Add(2*time.Second))
}

func (c *CloudConnection) RemoteAddr() net.Addr {
	return c.connection.RemoteAddr()
}

func (c *CloudConnection) ReceiveMessage() (*CloudMessage, error) {
	var m CloudMessage
	if err := c.connection.ReadJSON(&m); err != nil {
		return nil, err
	}
	log.Printf("WS In: %+v", m)
	return &m, nil
}
