package hub

type CloudHub struct {
	id2conn map[string]*CloudConnection
	conn2id map[*CloudConnection]string
}

const HUB_NAME = "hub"
